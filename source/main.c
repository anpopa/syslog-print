/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020 Alin Popa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 */

#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>

static char *conf_app_name = NULL;
static char *conf_ctx_name = NULL;
static int   conf_prio = LOG_NOTICE;
static int   conf_delay = 10;
static bool  conf_test = false;

static void terminate(int signum)
{
    exit(EXIT_SUCCESS);
}

static void do_run_test(void)
{
    char buffer[64] = {0};

    while (true) {
        snprintf(buffer, sizeof(buffer), "Random number %0X", random());
        usleep(conf_delay * 1000);
        syslog(conf_prio, buffer);
    }
        
}

static void do_log_string(const char *str)
{
    syslog(conf_prio, str);
}

static void do_log_stdin(void)
{
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, stdin)) != -1)
        syslog(conf_prio, line);

    free(line);
}

int main(int argc, char **argv)
{
    char hostname_buffer[64] = {0};
    int long_index = 0;
    bool single_line = false;
    bool help = false;
    char *ident = NULL;
    int ident_size;
    int c;

    struct option longopts[] = {{"app", required_argument, NULL, 'a'},
                                {"ctx", required_argument, NULL, 'c'},
                                {"prio", required_argument, NULL, 'p'},
                                {"delay", required_argument, NULL, 'd'},
                                {"test", no_argument, NULL, 't'},
                                {"help", no_argument, NULL, 'h'},
                                {NULL, 0, NULL, 0}};

    while ((c = getopt_long(argc, argv, "a:c:p:d:th", longopts, &long_index)) != -1) {
        switch (c) {
        case 'a':
            conf_app_name = strdup(optarg);
            break;
        case 'c':
            conf_ctx_name = strdup(optarg);
            break;
        case 'p':
            if (strcmp(optarg, "LOG_ERR") == 0)
                conf_prio = LOG_ERR;
            else if (strcmp(optarg, "LOG_WARNING") == 0)
                conf_prio = LOG_WARNING;
            else if (strcmp(optarg, "LOG_CRIT") == 0)
                conf_prio = LOG_CRIT;
            else if (strcmp(optarg, "LOG_DEBUG") == 0)
                conf_prio = LOG_DEBUG;
            break;
        case 'd':
            conf_delay = strtoul(optarg, NULL, 10);
            break;
        case 't':
            conf_test = true;
            break;
        case 'h':
            help = true;
            break;
        default:
            break;
        }
    }

    if (help) {
        printf("syslog-print: Print text data to syslog\n\n");
        printf("Usage: syslog-print [OPTIONS] STDIN\n\n");
        printf("  General:\n");
        printf("     --app, -a      <string> Application name. Default to syslog-print\n");
        printf("     --ctx, -c      <string> Context name. Default hostname\n");
        printf("     --prio, -p     <string> Log priotity (LOG_DEBUG, LOG_WARNING, LOG_CRIT, LOG_ERR)\n");
        printf("     --delay, -d    <uint>   Time in ms to wait between lines printed\n");
        printf("     --test, -t              Enable stress test mode (print random strings)\n");
        printf("  Help:\n");
        printf("     --help, -h              Print this help\n\n");
        exit(EXIT_SUCCESS);
    }

    if (conf_app_name == NULL)
        conf_app_name = strdup(argv[0]);

    gethostname(hostname_buffer, sizeof(hostname_buffer));
    if (conf_ctx_name == NULL)
        conf_ctx_name = strdup(hostname_buffer);

    signal(SIGINT, terminate);
    signal(SIGTERM, terminate);

    ident_size = strlen(conf_app_name) + strlen(conf_ctx_name) + 2;
    ident = calloc(1, ident_size);
    snprintf(ident, ident_size, "%s.%s", conf_ctx_name, conf_app_name);

    /* Open log explicitly */
    openlog(ident, LOG_NDELAY, LOG_USER);

    if (conf_test == true)
        do_run_test();
    else if (argc > optind)
        do_log_string(argv[optind]);
    else
        do_log_stdin();

    free (conf_app_name);
    free (conf_ctx_name);

    return EXIT_SUCCESS;
}